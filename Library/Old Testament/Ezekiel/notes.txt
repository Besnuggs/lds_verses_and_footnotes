**************************************** . ****************************************
31.HTML

{"note9a": ["Ezek. 28:13"], "note3b": ["Isa. 2:13"], "note3a": ["Isa. 10:5"], "note10a": ["Isa. 14:13 (1315)", "Ezek. 28:17"], "note2a": ["Isa. 19:1", "Ezek. 32:2"]}

35.HTML

{"note10a": ["Ezek. 36:2 (17)"], "note9a": ["Jer. 49:13"], "note8a": ["Ether 14:21 (2122)"], "note2a": ["Ezek. 6:2"], "note2b": ["Gen. 32:3", "36:8 (89)", "Deut. 2:1", "Isa. 21:11", "Ezek. 25:8"], "note15a": ["Obad. 1:15"], "note15b": ["TGWorld, End of"], "note12a": ["TGBlaspheme"], "note13a": ["Hel. 4:13"], "note5a": ["Ps. 137:7", "Ezek. 36:2 (17)", "Obad. 1:11 (1011)"]}

1.HTML

{"note11a": ["D&C 77:4"], "note27a": ["Ezek. 8:2"], "note4a": ["2Kgs. 2:11"], "note1a": ["TGIsrael, Bondage of, in Other Lands"], "note1c": ["Acts 7:56 (5556)", "1Ne. 1:8 (611)", "11:14", "D&C 107:19", "110:11"], "note1b": ["Ezek. 10:15"], "note1d": ["Ezek. 8:3", "40:2", "43:3", "1Ne. 1:16", "JSH 1:24 (2125)"], "note7a": ["Ezek. 40:3"], "note5a": ["TGSymbolism"], "note3a": ["2Kgs. 3:15", "Ezek. 8:1", "33:22", "37:1", "40:1"], "note15a": ["Dan. 7:9"], "note20a": ["Ezek. 10:17"], "note17a": ["Ezek. 10:11"], "note13a": ["Rev. 4:5"], "note28d": ["Isa. 6:3 (23)", "Rev. 4:8 (79)"], "note9a": ["Ezek. 10:11"], "note22d": ["Rev. 4:6", "D&C 77:1", "130:9 (79)"], "note22b": ["Ezek. 10:20"], "note22a": ["Ezek. 10:1"], "note26b": ["TGJesus Christ, Appearances, Antemortal"], "note26a": ["Ezek. 10:1"], "note24a": ["Ezek. 43:2", "Rev. 1:15", "Hel. 12:9", "3Ne. 11:3", "D&C 110:3"], "note2a": ["Ezek. 24:1", "26:1", "33:21"], "note2b": ["2Kgs. 24:12 (12, 15)"], "note28a": ["Rev. 4:3"], "note28c": ["Ezek. 11:22", "TGJesus Christ, Glory of"], "note28b": ["TGGod, Privilege of Seeing"], "note28e": ["Ezek. 3:23 (2324)", "44:4", "Acts 9:4 (35)", "Ether 3:6 (68)"], "note16a": ["Ezek. 10:9"]}

2.HTML

{"note9b": ["Rev. 5:1", "10:2", "D&C 77:6"], "note7a": ["Num. 24:13", "TGAuthority", "Prophets, Mission of"], "note9a": ["Ezra 6:2", "Jer. 36:2 (2, 4)"], "note3b": ["TGApostasy of Israel", "Rebellion"], "note3a": ["TGPriesthood, Keys of", "Priesthood, Ordination"], "note8a": ["Ezek. 3:3 (23)", "Rev. 10:10 (910)"], "note2a": ["TGTeaching with the Spirit"], "note5a": ["Ezek. 12:2", "44:6"], "note6a": ["Jer. 1:8"], "note6b": ["TGPeer Influence"], "note5b": ["Ezek. 33:33"], "note4b": ["TGCalled of God"], "note4a": ["TGStiffnecked", "Stubbornness"]}

48.HTML

{"note1a": ["TGIsrael, Twelve Tribes of"], "note28a": ["Num. 20:13"], "note1b": ["Num. 2:25 (2531)"], "note11a": ["Ezek. 43:19", "45:4"], "note11b": ["Ezek. 40:45", "44:14 (8, 1416)"], "note29a": ["Num. 34:2 (215)", "Ezek. 47:14 (1421)", "2Ne. 10:7 (78)"], "note15a": ["Ezek. 42:20", "45:6"], "note15b": ["Num. 35:2 (15)", "Ezek. 45:2"], "note31a": ["Rev. 21:12 (1213)"], "note10a": ["Ezek. 45:3"], "note31b": ["Neh. 11:1 (12)"], "note35a": ["Rev. 21:3"], "note31c": ["Deut. 33:6 (68)", "1Chr. 5:3 (1, 3, 18)", "Rev. 7:5"], "note21a": ["Ezek. 44:3", "45:7"], "note13a": ["Ezek. 45:5"], "note14a": ["Lev. 27:28 (10, 28, 33)"], "note5a": ["Ether 13:6 (512)"], "note4a": ["Ezek. 47:13"]}

21.HTML

{"note28a": ["Jer. 49:2 (12)"], "note31c": ["D&C 43:26"], "note7a": ["Ezek. 7:17"], "note27a": ["TGJesus Christ, Second Coming"], "note20a": ["Jer. 49:2", "Ezek. 25:5 (45)", "Amos 1:14"], "note3a": ["D&C 1:13"], "note2a": ["Ezek. 20:46"], "note22a": ["Ezek. 4:2"], "note2c": ["TGIsrael, Land of"], "note23a": ["Ezek. 17:20 (20, 24)"], "note26b": ["Luke 1:52"], "note31a": ["TGGod, Indignation of"], "note27c": ["Gen. 49:10"], "note31b": ["Ezek. 22:21 (2021)"], "note17a": ["Ezek. 5:13", "3Ne. 21:21"], "note25b": ["Ezek. 22:3"], "note25a": ["2Chr. 36:12 (1213)"], "note24a": ["D&C 3:9"]}

20.HTML

{"note11a": ["Deut. 4:8", "Neh. 9:13"], "note11b": ["2Ne. 1:16 (1617)", "5:10 (1011)"], "note37c": ["3Ne. 29:1 (13)", "Morm. 5:14 (14, 20)"], "note8a": ["Ezek. 7:8"], "note35c": ["TGGod, Presence of"], "note23a": ["TGIsrael, Scattering of"], "note35a": ["Hosea 2:14"], "note27a": ["Isa. 65:7"], "note39b": ["D&C 1:16"], "note21a": ["TGRebellion"], "note6a": ["TGIsrael, Deliverance of"], "note6b": ["Abr. 2:6"], "note6d": ["Ps. 48:2"], "note4a": ["Ezek. 16:2"], "note1a": ["Ezek. 33:21"], "note1b": ["TGElder"], "note7a": ["Ezek. 18:31"], "note19a": ["Deut. 5:31", "6:1"], "note3a": ["1Sam. 8:18", "Ezek. 14:3", "Mosiah 21:15", "D&C 101:7"], "note38a": ["Ezek. 34:17 (17, 20, 22)"], "note15a": ["TGIsrael, Land of"], "note39a": ["Ezek. 39:7", "43:7"], "note17a": ["Ezek. 5:11"], "note13c": ["Num. 14:29", "26:65"], "note13b": ["1Ne. 17:30 (3031)", "Jacob 4:14"], "note13a": ["1Cor. 10:5 (510)"], "note40c": ["Isa. 60:7", "Mal. 3:4"], "note40b": ["TGIsrael, Twelve Tribes of"], "note40a": ["Isa. 56:7"], "note10a": ["Ex. 13:18 (18, 20)"], "note43a": ["Mosiah 2:40", "Alma 5:18 (718)"], "note9a": ["Ex. 9:16", "Ps. 106:8", "Isa. 48:11", "63:14", "Ezek. 36:21 (2124)"], "note46a": ["Ezek. 6:2", "21:2"], "note20b": ["TGSigns"], "note34a": ["Deut. 30:3 (15)"], "note35b": ["Ezek. 17:20"], "note26c": ["Hel. 12:3"], "note26b": ["2Kgs. 21:6", "2Chr. 28:3", "33:6"], "note26d": ["TGGod, Knowledge about"], "note32b": ["TGTraditions of Men"], "note32c": ["TGIdolatry"], "note32a": ["Ps. 94:11", "Ezek. 11:5"], "note18a": ["Hel. 15:4", "D&C 93:39"], "note5a": ["TGIsrael, Mission of"], "note38b": ["TGRebellion"], "note5c": ["Ex. 20:2"], "note37b": ["Lev. 27:32"], "note49a": ["TGProphets, Rejection of"], "note16a": ["TGHeart"], "note12a": ["TGSabbath"], "note12b": ["TGSanctification"], "note38c": ["Ezek. 13:9"], "note38d": ["TGIsrael, Land of"], "note41a": ["TGIsrael, Gathering of"], "note41b": ["TGSanctification"], "note42a": ["3Ne. 16:4 (45)", "20:13"]}

44.HTML

{"note11a": ["TGTemple"], "note11b": ["Ezek. 45:5", "46:24"], "note23d": ["TGUncleanness"], "note23a": ["Mosiah 23:14"], "note23b": ["Lev. 10:10", "Ezek. 22:26", "TGDiscernment, Spiritual"], "note23c": ["TGSacred"], "note24e": ["Jer. 17:27 (22, 24, 27)", "Ezek. 22:26"], "note31a": ["Lev. 22:8"], "note21b": ["Lev. 10:9"], "note21a": ["TGPriest, Aaronic Priesthood", "BDPriests"], "note6a": ["Ezek. 2:5", "12:2", "24:3"], "note25a": ["Lev. 21:1 (13)"], "note4b": ["Ex. 40:34 (3435)", "1Kgs. 8:11 (1011)", "Ezek. 43:5"], "note4c": ["Ezek. 1:28"], "note4a": ["Ezek. 40:20 (17, 20)"], "note1a": ["Ezek. 40:6", "42:15", "43:1 (12)"], "note7f": ["D&C 1:15"], "note7e": ["TGBlood, Eating of"], "note7d": ["Lev. 3:17 (1617)"], "note7c": ["Lev. 3:11"], "note7b": ["TGSacrilege"], "note7a": ["Jer. 51:51", "Ezek. 23:38"], "note3b": ["Gen. 31:54"], "note3a": ["Ezek. 34:24", "37:25 (2425)", "45:7", "48:21"], "note15a": ["Deut. 17:9", "Ezek. 43:19", "TGPriest, Aaronic Priesthood", "BDPriests"], "note20a": ["Lev. 21:5", "1Cor. 11:14"], "note15c": ["Num. 16:5", "Ezek. 40:46"], "note15b": ["Deut. 10:8"], "note17b": ["Ex. 28:39", "39:27"], "note17a": ["Ex. 28:43"], "note13c": ["TGShame"], "note13b": ["Ezek. 41:4 (14)", "45:3"], "note13a": ["Num. 18:3", "2Kgs. 23:9"], "note29a": ["Lev. 6:14 (1418, 2529)"], "note9a": ["TGCircumcision"], "note22b": ["Matt. 5:32"], "note22a": ["Lev. 21:14 (7, 1314)"], "note30a": ["Ex. 22:29", "Neh. 10:35"], "note26a": ["Num. 19:11 (1112)"], "note30c": ["Num. 15:20"], "note18b": ["Lev. 6:10"], "note24d": ["TGMeetings"], "note18a": ["Ex. 39:28"], "note24a": ["Deut. 17:8 (89)"], "note24c": ["Deut. 25:1 (13)"], "note24b": ["Moro. 7:15 (1518)"], "note2a": ["Ezek. 43:4"], "note28b": ["Num. 18:20"], "note16a": ["Ezek. 41:22", "Mal. 1:12 (7, 12)"], "note19a": ["Ezek. 42:14"], "note14a": ["Ezek. 40:45", "48:11"]}

12.HTML

{"note11a": ["TGSigns"], "note11b": ["Ezek. 24:24"], "note27a": ["Amos 6:3"], "note25b": ["TGPromise"], "note6b": ["Ezek. 24:24", "TGSigns"], "note25a": ["Isa. 55:11"], "note19b": ["Ps. 107:34"], "note15a": ["TGIsrael, Scattering of"], "note13c": ["Jer. 39:7", "52:11"], "note13b": ["TGIsrael, Bondage of, in Other Lands"], "note13a": ["Ezek. 17:20", "32:3", "Hosea 7:12"], "note9a": ["Ezek. 17:12"], "note9b": ["Ezek. 24:19", "37:18"], "note24a": ["TGFlatter"], "note24b": ["TGSorcery"], "note2a": ["Ezek. 2:5", "44:6"], "note2b": ["D&C 76:116 (11618)"], "note2c": ["TGSpiritual Blindness", "Watch"], "note16a": ["TGIsrael, Remnant of"], "note14b": ["Ezek. 17:21"], "note14c": ["Ezek. 5:2"], "note14a": ["2Kgs. 25:5"]}

22.HTML

{"note27a": ["TGKings, Earthly"], "note27b": ["2Ne. 26:29", "4Ne. 1:26"], "note21a": ["Ezek. 21:31"], "note25b": ["TGFalse Prophets"], "note25c": ["Prov. 28:15"], "note25a": ["TGConspiracy"], "note25d": ["TGTreasure"], "note4a": ["Ezek. 5:14"], "note7e": ["Amos 5:12", "Hel. 6:39 (3740)"], "note7d": ["TGStranger"], "note7c": ["Zech. 7:10 (911)"], "note7b": ["TGHonoring Father and Mother"], "note3c": ["Ezek. 21:25"], "note3b": ["TGBlood, Shedding of"], "note3a": ["Ezek. 36:18 (1620)"], "note15a": ["TGIsrael, Scattering of", "Israel, Ten Lost Tribes of"], "note15b": ["TGFilthiness"], "note13a": ["TGHonesty"], "note25e": ["Jer. 20:5"], "note29a": ["TGPoor"], "note10c": ["Lev. 18:19"], "note10a": ["Lev. 18:7 (78)"], "note10d": ["Ezek. 36:17 (1620)"], "note9b": ["TGSensuality"], "note26c": ["Lev. 10:10", "Ezek. 44:23"], "note26b": ["Mal. 2:8"], "note26a": ["TGApostasy of Israel"], "note26e": ["TGProfanity"], "note26d": ["Jer. 17:27 (22, 24, 27)", "Ezek. 44:24"], "note18b": ["Isa. 48:10"], "note18a": ["Ps. 119:119", "TGApostasy of Israel", "Wickedness"], "note24a": ["Ps. 68:9", "Ezek. 34:26"], "note24b": ["TGGod, Indignation of"], "note30a": ["Gen. 18:26", "Jer. 5:1"], "note31a": ["Ezek. 7:4"], "note28a": ["Ezek. 13:10"], "note28b": ["TGSuperstitions"], "note12a": ["Deut. 27:25", "Isa. 1:23"], "note12b": ["Isa. 3:14", "Amos 3:10", "Alma 4:12 (1213)", "TGUsury"]}

34.HTML

{"note11a": ["TGMissionary Work"], "note8b": ["2Ne. 28:13 (916)", "Morm. 8:37 (3741)"], "note8a": ["TGApostasy of Israel"], "note23a": ["Ezek. 37:22"], "note23b": ["Isa. 55:4", "TGShepherd"], "note23c": ["Ezek. 37:24", "TGMillennium"], "note27a": ["TGEarth, Destiny of"], "note27b": ["Ps. 119:117"], "note27c": ["TGBondage, Physical", "Bondage, Spiritual"], "note25b": ["Ezek. 38:8", "D&C 45:68 (6870)"], "note6a": ["TGSheep"], "note25a": ["Ezek. 37:26"], "note4b": ["TGCruelty"], "note4a": ["TGSickness"], "note28a": ["Ezek. 28:26"], "note19a": ["1Ne. 19:7"], "note3a": ["2Ne. 26:29"], "note17b": ["Ezek. 20:38"], "note17a": ["TGSheep"], "note13c": ["Deut. 18:9"], "note13b": ["D&C 33:6 (37)"], "note13a": ["Ezek. 38:8", "TGMillennium, Preparing a People for"], "note29b": ["Ezek. 32:24", "36:6", "TGShame"], "note29a": ["Isa. 61:3"], "note10b": ["D&C 107:100 (99100)"], "note10a": ["Jacob 1:19", "D&C 72:3", "112:33"], "note22a": ["2Ne. 25:16 (1518)"], "note26b": ["TGIsrael, Blessings of"], "note26a": ["Ps. 68:9", "Ezek. 22:24"], "note24d": ["Ezek. 37:25 (2425)", "44:3"], "note18a": ["TGPollution"], "note24c": ["Jer. 23:5 (56)", "30:9", "Hosea 3:5"], "note24b": ["Ex. 29:45 (4546)", "Lev. 26:12 (1113)", "Ezek. 37:27 (2228)"], "note2a": ["Isa. 56:11", "TGShepherd"], "note2b": ["TGPriesthood, Magnifying Callings within"], "note16d": ["Jer. 10:24"], "note31a": ["TGSheep"], "note16a": ["Luke 15:4", "19:10"], "note16b": ["TGSickness"], "note16c": ["Isa. 10:16"], "note12a": ["TGShepherd"], "note12b": ["TGIsrael, Gathering of"], "note12c": ["TGJesus Christ, Good Shepherd"], "note12d": ["2Ne. 25:15 (1119)"], "note12e": ["Ezek. 30:3"], "note12f": ["Joel 2:2", "Matt. 24:29"], "note14a": ["Jer. 33:12"]}

42.HTML

{"note1a": ["Ezek. 40:20"], "note20b": ["Ezek. 45:2"], "note20a": ["Ezek. 40:5"], "note3b": ["Ezek. 41:16 (1516)"], "note20c": ["Ezek. 45:6", "48:15"], "note8a": ["Ezek. 40:47", "41:13"], "note14a": ["Ezek. 44:19"], "note15a": ["Ezek. 40:6", "43:1 (12)", "44:1"], "note16a": ["Ezek. 40:3"], "note13c": ["Lev. 2:1 (1, 3, 10)"], "note13b": ["Lev. 6:26 (16, 26)", "10:13"], "note13a": ["Ezek. 40:46"], "note13d": ["Lev. 4:3 (23)", "Ezek. 40:39"], "note4a": ["Ezek. 40:17", "41:10", "46:19"]}

27.HTML

{"note10a": ["Isa. 66:19", "Jer. 46:9", "Ezek. 30:5"], "note7a": ["Gen. 10:4"], "note3a": ["Lam. 2:15", "Ezek. 28:12"], "note2a": ["Ezek. 28:12 (1112)"], "note34a": ["Ezek. 26:19"], "note15a": ["Ezek. 39:6"], "note30a": ["Rev. 18:19"], "note26a": ["Gen. 41:23", "Ps. 78:26", "Jer. 18:17", "Mosiah 7:31", "12:6"], "note17b": ["Judg. 11:33"], "note17a": ["TGIsrael, Land of"], "note12a": ["1Kgs. 10:22", "Ezek. 38:13"], "note6a": ["Zech. 11:2"], "note13a": ["Gen. 10:2 (23)", "Ps. 120:5", "Ezek. 38:2"], "note25a": ["Ps. 48:7", "Isa. 2:16"], "note5a": ["Deut. 3:9 (89)"], "note14a": ["Gen. 10:3 (23)", "Ezek. 38:6"]}

38.HTML

{"note8b": ["Ezek. 34:13"], "note8c": ["D&C 133:13"], "note23a": ["TGSanctification"], "note8d": ["Ezek. 34:25"], "note21a": ["Isa. 9:19"], "note6a": ["Gen. 10:2 (23)"], "note6b": ["Gen. 10:3 (23)", "Ezek. 27:14"], "note19a": ["Hag. 2:6 (67)"], "note4a": ["Ezek. 29:4"], "note9b": ["Jer. 4:13"], "note15a": ["Ezek. 39:2"], "note13a": ["1Kgs. 10:22", "Ezek. 27:12"], "note10a": ["TGMind"], "note9a": ["Isa. 28:2"], "note22d": ["Ezek. 39:6", "D&C 29:21", "TGWorld, End of"], "note22c": ["Ex. 9:18 (1335)", "Josh. 10:11", "Rev. 16:21", "D&C 29:16"], "note22b": ["TGLast Days"], "note22a": ["Isa. 66:16", "Jer. 25:31", "Joel 3:2", "Zech. 14:3"], "note23b": ["Ezek. 39:7"], "note18a": ["Ezek. 5:13"], "note2a": ["Gen. 10:2 (23)", "Ezek. 27:13", "39:1", "Rev. 20:8"], "note16a": ["Luke 21:20 (2024)", "Rev. 16:16"], "note16b": ["Jer. 30:24"], "note16c": ["TGConversion"]}

36.HTML

{"note11a": ["Ezek. 16:55"], "note8b": ["Isa. 56:1", "D&C 4:4 (17)"], "note8a": ["Jacob 5:3 (377)"], "note23a": ["TGConversion"], "note23b": ["TGSanctification"], "note27a": ["Isa. 44:3", "Acts 2:17", "D&C 46:28 (28, 30)", "84:46", "95:4"], "note31a": ["Ezek. 16:61"], "note21b": ["Ezek. 20:9"], "note19d": ["Ezek. 39:24"], "note25b": ["TGFilthiness"], "note6a": ["Ezek. 32:24", "34:29"], "note36a": ["Ps. 126:2", "Ezek. 17:24"], "note25a": ["Isa. 52:15", "3Ne. 20:45"], "note19a": ["Hel. 3:16"], "note4c": ["Ps. 44:13"], "note19c": ["Jer. 25:34"], "note19b": ["TGHeathen"], "note1a": ["Ezek. 6:2"], "note4b": ["Ezek. 33:24"], "note24a": ["TGIsrael, Gathering of"], "note20a": ["TGBlaspheme"], "note17b": ["Ezek. 22:10"], "note17a": ["Lev. 18:25", "2Ne. 13:8", "25:14"], "note13a": ["Jer. 15:7", "Ezek. 5:17"], "note10b": ["Amos 9:14"], "note10c": ["Isa. 61:4"], "note37a": ["Ps. 102:17", "Jer. 29:13", "Ezek. 14:3", "3Ne. 13:8"], "note10a": ["Zech. 2:4"], "note9a": ["D&C 88:63 (6364)"], "note35a": ["Isa. 51:3", "TGMillennium"], "note22b": ["Ps. 106:8"], "note22a": ["Deut. 9:5"], "note34a": ["Isa. 35:1"], "note26c": ["TGGod, Gifts of"], "note26b": ["3Ne. 10:6"], "note26a": ["TGMan, New, Spiritually Reborn"], "note36b": ["D&C 62:6"], "note34b": ["Isa. 61:4 (46)"], "note26e": ["TGConversion"], "note26d": ["TGSpirituality"], "note18b": ["2Ne. 20:11"], "note18c": ["Ezek. 16:36"], "note18a": ["Ezek. 22:3"], "note5a": ["Deut. 4:24"], "note24b": ["TGIsrael, Land of"], "note25c": ["Ezek. 37:23"], "note31b": ["Ezek. 6:9"], "note2a": ["Ezek. 35:5"], "note2b": ["Ezek. 35:10"], "note28a": ["Ezek. 28:25", "37:12 (1213)", "25"], "note12a": ["1Ne. 10:3"]}

23.HTML

{"note37a": ["TGAdulterer"], "note37b": ["Lev. 17:7", "20:5", "Jer. 3:9 (89)"], "note35a": ["TGUnbelief"], "note31a": ["Jer. 25:15"], "note25a": ["2Ne. 6:8"], "note33a": ["Jer. 13:13"], "note24a": ["2Kgs. 25:6 (67)"], "note17a": ["TGFilthiness"], "note39b": ["2Kgs. 21:4", "D&C 121:19"], "note39a": ["TGSacrilege"], "note40a": ["Hosea 2:13"], "note45a": ["Ezek. 16:38"], "note47a": ["Ezek. 24:21"], "note22b": ["Ezek. 16:37"], "note30a": ["Ezek. 6:9"], "note26a": ["Ezek. 16:39"], "note12a": ["2Kgs. 16:7 (718)", "2Chr. 28:16 (1621)", "Ezek. 16:28"], "note5a": ["2Kgs. 15:19", "17:6 (36)", "Hosea 8:9"], "note49a": ["TGIdolatry"], "note2a": ["Jer. 3:7 (78, 10)", "Ezek. 16:46 (4546, 61)"], "note28a": ["Ezek. 16:37 (3741)"], "note38a": ["Jer. 51:51", "Ezek. 44:7"]}

4.HTML

{"note2d": ["Jer. 6:6"], "note2e": ["Ezek. 21:22"], "note3a": ["2Ne. 25:9"], "note2b": ["2Kgs. 25:1"], "note17b": ["Hel. 12:3"], "note14d": ["Lev. 7:18", "19:7"], "note13a": ["Hosea 9:3 (13)"], "note14b": ["Lev. 7:24"], "note14c": ["Ex. 22:31"], "note14a": ["TGPollution"]}

28.HTML

{"note7a": ["Ezek. 30:11"], "note24a": ["Num. 33:55", "Josh. 23:13"], "note3a": ["Ezek. 14:14", "Dan. 9:22 (2223)"], "note2a": ["TGPride"], "note2b": ["Isa. 55:8 (89)"], "note26a": ["Jer. 23:6", "Ezek. 34:28 (25, 28)"], "note17a": ["Ezek. 31:10"], "note21a": ["Isa. 23:4 (412)", "Jer. 25:22", "27:3"], "note25b": ["TGIsrael, Ten Lost Tribes of"], "note12a": ["Ezek. 27:2", "Ether 15:16"], "note12b": ["Lam. 2:15", "Ezek. 27:3"], "note25a": ["TGIsrael, Gathering of"], "note5a": ["2Ne. 9:28 (2829)", "42", "Hel. 1:16"], "note13a": ["Ezek. 31:9 (89)"], "note25d": ["Ezek. 36:28", "37:25"], "note14a": ["TGCherubim"]}

18.HTML

{"note11a": ["Lev. 20:10"], "note8a": ["TGUsury"], "note23a": ["Ezek. 33:11", "1Tim. 2:4 (4, 6)", "2Pet. 3:9", "TGPleasure"], "note31c": ["TGMan, New, Spiritually Reborn"], "note27a": ["Ezek. 33:14"], "note31a": ["Ezek. 20:7"], "note21a": ["TGRepent"], "note6a": ["Ps. 123:1"], "note6b": ["2Ne. 9:37"], "note6c": ["D&C 42:24 (2226)"], "note6d": ["Lev. 18:19"], "note4a": ["TGJustice", "Sin"], "note25a": ["Ezek. 33:17"], "note7b": ["Alma 34:28 (1829)"], "note7a": ["Ex. 22:26", "Deut. 24:13", "Ezek. 33:15"], "note30b": ["TGTransgress"], "note4b": ["TGPunish"], "note31b": ["TGConversion", "Heart"], "note13a": ["Ezek. 33:4"], "note31d": ["Ezek. 33:11"], "note20a": ["TGSin"], "note20c": ["TGRighteousness"], "note20b": ["TGAccountability", "Punish"], "note22a": ["TGForgive", "Transgress"], "note26b": ["1Ne. 15:33 (3233)", "Mosiah 15:26", "Moro. 10:26"], "note26a": ["TGApostasy of Individuals"], "note24d": ["2Ne. 9:38"], "note18a": ["TGCruelty"], "note24a": ["Ezek. 3:20"], "note24c": ["TGRighteousness"], "note24b": ["Ezek. 33:12 (1213, 18)"], "note30a": ["Ezek. 7:3 (3, 8)", "33:20"], "note2a": ["Jer. 31:29", "Lam. 5:7"], "note5a": ["Heb. 12:23", "D&C 76:69 (6669)"], "note12a": ["2Ne. 13:14 (1415)", "28:13 (1213)", "Hel. 4:12 (1113)"]}

19.HTML

{"note1a": ["Jer. 7:29"], "note10a": ["TGVineyard of the Lord"], "note8b": ["Hel. 4:26"], "note8c": ["Lam. 4:20"], "note3a": ["2Chr. 36:1"], "note8a": ["2Kgs. 24:2"], "note2a": ["Gen. 49:812"], "note9a": ["2Chr. 36:6"], "note14b": ["Ezek. 17:13 (1321)"], "note12a": ["2Chr. 36:20 (1720)"], "note6a": ["Jer. 22:13 (1318)"], "note12b": ["Ezek. 17:10"], "note14a": ["Judg. 9:15 (120)"], "note4b": ["2Chr. 36:4 (34)"], "note13a": ["2Kgs. 24:15 (1216)"], "note4a": ["2Kgs. 23:33 (3334)"]}

15.HTML

{"note6a": ["TGVineyard of the Lord"], "note6b": ["1Ne. 1:13"]}

30.HTML

{"note11a": ["Ezek. 28:7"], "note9a": ["Isa. 20:4"], "note3a": ["Ezek. 34:12", "D&C 109:61"], "note23a": ["Jer. 46:19", "Ezek. 29:12"], "note13c": ["Zech. 10:11"], "note12a": ["Isa. 19:5", "Jer. 51:36"], "note13a": ["Jer. 43:12"], "note5a": ["Zeph. 2:12"], "note5c": ["Isa. 66:19", "Jer. 46:9", "Ezek. 27:10"], "note14a": ["Jer. 46:25", "Nahum 3:8"]}

6.HTML

{"note9b": ["TGRepent"], "note7a": ["TGGod, Knowledge about"], "note8b": ["Jer. 44:28", "Ezek. 7:16"], "note8c": ["TGIsrael, Scattering of"], "note9c": ["Ezek. 36:31"], "note8a": ["TGIsrael, Remnant of"], "note2a": ["Ezek. 20:46", "35:2 (115)"], "note2b": ["Ezek. 36:1 (17)"], "note9a": ["Ezek. 23:30"], "note13a": ["TGIdolatry"], "note5a": ["Ps. 141:7"]}

11.HTML

{"note23a": ["Ezek. 43:2 (16)"], "note21b": ["Ezek. 7:4"], "note21a": ["Deut. 28:15 (1568)"], "note19e": ["2Cor. 3:3 (23)"], "note19d": ["TGSpirituality"], "note19a": ["TGGod, Gifts of"], "note19c": ["TGMan, New, Spiritually Reborn"], "note19b": ["TGConversion"], "note1a": ["TGGod, Spirit of", "Guidance, Divine"], "note1b": ["Ezek. 10:19"], "note6a": ["TGBlood, Shedding of"], "note24a": ["TGGod, Spirit of"], "note17c": ["TGIsrael, Land of"], "note17a": ["TGIsrael, Gathering of"], "note10a": ["2Kgs. 25:21 (1921)", "Jer. 39:6", "52:10"], "note20a": ["TGWalking with God"], "note20c": ["TGIsrael, Mission of"], "note20b": ["TGOrdinance"], "note22a": ["Ezek. 1:28"], "note5a": ["TGHoly Ghost, Mission of"], "note5c": ["Ezek. 20:32", "TGMind"], "note5b": ["TGGod, Omniscience of"], "note16a": ["Isa. 8:14", "TGRefuge"], "note12a": ["Ezek. 5:7"], "note12b": ["TGApostasy of Israel"], "note12c": ["TGHeathen"]}

33.HTML

{"note11a": ["Ezek. 18:23", "Mosiah 26:30", "29:20", "Moses 7:39"], "note11c": ["Ezek. 18:31"], "note11b": ["TGGod, Mercy of"], "note31c": ["Ps. 119:36", "Isa. 57:17"], "note27a": ["Ezek. 5:17", "14:21", "Rev. 6:8"], "note31a": ["Matt. 11:15", "Heb. 5:11 (1114)", "2Ne. 9:31", "D&C 1:14"], "note21b": ["Ezek. 24:26"], "note21c": ["TGJerusalem"], "note21a": ["Ezek. 1:2", "20:1", "24:1", "26:1"], "note6a": ["D&C 88:81 (8182)"], "note6b": ["TGAccountability"], "note25a": ["TGBlood, Eating of"], "note4b": ["TGAccountability"], "note4a": ["Ezek. 18:13", "Acts 18:6"], "note7a": ["TGLeadership", "Watchman"], "note33a": ["Ezek. 2:5"], "note3a": ["TGWarn"], "note15a": ["TGReconciliation", "Repent"], "note20a": ["Ezek. 18:30", "1Ne. 10:20"], "note15c": ["Lev. 18:5"], "note15b": ["Ezek. 18:7"], "note17c": ["TGJustice"], "note17a": ["Ezek. 18:25 (25, 27)"], "note13b": ["TGApostasy of Individuals"], "note13a": ["TGTrustworthiness"], "note9a": ["TGPriesthood, Magnifying Callings within", "Prophets, Mission of"], "note9b": ["1Ne. 10:21"], "note22b": ["Ezek. 24:27"], "note22a": ["Ezek. 1:3"], "note18b": ["TGRighteousness"], "note18a": ["TGApostasy of Individuals"], "note24a": ["Ezek. 36:4"], "note31b": ["Isa. 29:13", "Matt. 15:8 (78)", "Luke 6:46", "Alma 34:28", "JSH 1:19"], "note2a": ["3Ne. 16:18", "D&C 101:45 (45, 5354)"], "note16a": ["TGForgive"], "note12a": ["Ezek. 3:20", "18:24"], "note14b": ["Ezek. 18:27"], "note14a": ["Ezek. 3:18"]}

16.HTML

{"note50b": ["Gen. 19:24 (2429)"], "note50a": ["Jer. 23:14", "TGHomosexual Behavior"], "note37a": ["Ezek. 23:28 (2829)"], "note37c": ["Lam. 1:8"], "note37b": ["Ezek. 23:22"], "note46a": ["Jer. 3:7 (78, 10)", "Ezek. 23:2"], "note46b": ["Isa. 1:10"], "note25b": ["Jacob 2:33 (2535)"], "note33a": ["Hosea 8:9"], "note4a": ["Hosea 2:3"], "note7b": ["Hosea 2:3 (3, 812)"], "note48a": ["Matt. 10:15", "11:24"], "note61b": ["TGRepent"], "note3b": ["Ezek. 16:45"], "note15a": ["Hosea 4:15 (1415)"], "note20a": ["Isa. 57:5"], "note63a": ["Rom. 3:19"], "note17a": ["TGIdolatry"], "note39a": ["Ezek. 23:26"], "note52a": ["Rom. 2:3"], "note52b": ["TGShame"], "note45a": ["Ezek. 16:3"], "note8a": ["TGLove"], "note43a": ["Ezek. 7:4"], "note9a": ["TGRemission of Sins"], "note60a": ["TGCovenants"], "note61a": ["Ezek. 36:31"], "note55a": ["Ezek. 36:11 (815)"], "note36b": ["Ezek. 36:18 (1620)"], "note36a": ["TGFilthiness"], "note32a": ["Hosea 2:7", "Joel 1:8"], "note59a": ["D&C 84:40 (3944)"], "note49a": ["Gen. 13:13"], "note49b": ["TGPride"], "note49c": ["TGIdleness", "Laziness"], "note2a": ["Ezek. 20:4", "D&C 88:81"], "note28a": ["2Kgs. 16:7 (718)", "2Chr. 28:16 (1621)", "Ezek. 23:12"], "note19a": ["Hosea 2:8"], "note51a": ["Jer. 3:11"], "note38a": ["Ezek. 23:45"], "note41a": ["2Kgs. 25:9", "Jer. 39:8", "52:13"], "note14a": ["Lam. 2:15"]}

17.HTML

{"note8a": ["Jacob 5:25, 43"], "note21b": ["Zech. 2:6"], "note21a": ["Ezek. 12:14"], "note6a": ["TGVineyard of the Lord"], "note4a": ["TGIsrael, Scattering of"], "note24a": ["2Ne. 20:33", "D&C 112:8 (38)"], "note3a": ["Jer. 48:40"], "note15a": ["2Kgs. 24:20", "2Chr. 36:13", "Jer. 52:3"], "note17b": ["Jer. 37:7"], "note17a": ["Jer. 44:30"], "note13c": ["2Kgs. 24:15"], "note13b": ["2Chr. 36:13"], "note13a": ["Ezek. 19:14"], "note10b": ["Ezek. 19:12"], "note10a": ["Jer. 18:17"], "note20a": ["Lam. 1:13", "Ezek. 12:13", "Hosea 7:12"], "note20c": ["TGIsrael, Bondage of, in Other Lands"], "note20b": ["Ezek. 21:23"], "note22c": ["TGIsrael, Scattering of"], "note20d": ["Ezek. 20:35"], "note22a": ["Omni 1:15 (1417)", "Mosiah 25:2", "Hel. 8:21"], "note22b": ["Jacob 5:24 (2224)"], "note5a": ["TGIsrael, Scattering of"], "note24b": ["Ezek. 36:36"], "note16a": ["TGIsrael, Bondage of, in Other Lands"], "note12a": ["Ezek. 12:9"], "note12b": ["2Kgs. 24:11 (1112)", "25:6 (17)", "Omni 1:15"]}

40.HTML

{"note44b": ["1Chr. 6:32 (3133)"], "note46d": ["Num. 16:5", "Ezek. 44:15 (1516)", "45:4"], "note46a": ["Ezek. 42:13"], "note46c": ["Ezek. 43:19"], "note46b": ["Num. 18:5"], "note6a": ["Ezek. 42:15", "43:1 (12)", "44:1", "47:2"], "note4b": ["Ezek. 43:10"], "note4a": ["D&C 8:2 (23)"], "note1a": ["Ezek. 1:3"], "note3c": ["Ezek. 45:1"], "note3b": ["Ezek. 42:16 (1619)", "47:3", "Rev. 11:1"], "note3a": ["Ezek. 1:7"], "note17a": ["Ezek. 41:10", "42:4 (1, 45)"], "note39c": ["Lev. 5:6 (56)", "Ezek. 46:20"], "note39b": ["Lev. 4:3 (23)", "Ezek. 42:13"], "note39a": ["Lev. 1:3 (3, 9, 1314)"], "note45a": ["Ezek. 44:14 (8, 1416)", "48:11"], "note47a": ["Ezek. 41:13", "42:8"], "note20a": ["Ezek. 42:1"], "note20b": ["Ezek. 44:4"], "note5e": ["Rev. 21:16"], "note5d": ["Ezek. 42:20"], "note5a": ["TGTemple"], "note5b": ["Ezek. 41:8", "43:13"], "note49a": ["1Kgs. 6:3"], "note49b": ["1Kgs. 7:21"], "note2a": ["Ezek. 1:1", "8:3"], "note2b": ["Ezek. 43:12", "Rev. 21:10", "Moses 1:1"], "note16a": ["Ezek. 41:18"], "note38a": ["2Chr. 4:6"], "note38b": ["Ezek. 46:2"]}

43.HTML

{"note11a": ["TGGenealogy and Temple Work", "Temple"], "note11b": ["TGOrdinance"], "note8a": ["D&C 63:61 (6064)"], "note27a": ["Lev. 9:1"], "note27b": ["TGSacrifice"], "note21b": ["Ex. 29:14"], "note19d": ["Lev. 8:14"], "note25a": ["Ex. 29:35 (3536)", "Lev. 8:33"], "note19a": ["Deut. 17:9", "Ezek. 44:15", "48:11"], "note19c": ["Ex. 29:10"], "note4a": ["Ezek. 44:2"], "note1a": ["Ezek. 40:6", "42:15", "44:1"], "note7b": ["Ezek. 20:39", "39:7"], "note7a": ["Isa. 60:13", "Matt. 5:35"], "note4b": ["TGTemple"], "note3a": ["Ezek. 1:1"], "note24a": ["TGSalt"], "note13a": ["Ezek. 40:5", "41:8"], "note19b": ["Ezek. 40:46"], "note10a": ["Ezek. 40:4"], "note18a": ["Lev. 1:5"], "note5a": ["TGGod, Spirit of"], "note5b": ["1Kgs. 8:11 (1011)", "Ezek. 44:4"], "note2d": ["D&C 133:22"], "note2e": ["Rev. 18:1"], "note2f": ["D&C 94:8", "101:25"], "note2a": ["Ezek. 11:23", "Rev. 21:11"], "note2b": ["Matt. 24:27"], "note2c": ["Ezek. 1:24", "Rev. 1:15", "D&C 110:3"], "note12a": ["Ezek. 40:2"]}

32.HTML

{"note7c": ["JSM 1:33"], "note7b": ["Isa. 13:10", "Joel 2:10", "Matt. 24:29"], "note9a": ["JSM 1:8"], "note3a": ["Ezek. 12:13"], "note2a": ["Isa. 19:1", "Ezek. 31:2"], "note2c": ["TGPollution"], "note27a": ["Isa. 14:18 (1819)"], "note27b": ["Gal. 6:7"], "note21a": ["TGHell"], "note18a": ["TGSpirits in Prison"], "note24a": ["Jer. 49:34 (3439)"], "note24b": ["Ezek. 34:29", "36:6"]}

24.HTML

{"note23a": ["Mosiah 7:24", "Hel. 9:22"], "note27a": ["Ezek. 3:26 (2627)", "33:22"], "note21b": ["Ps. 103:13"], "note21c": ["Ezek. 23:47"], "note21a": ["Jer. 7:14", "Ezek. 7:22 (2122)"], "note6a": ["TGBlood, Shedding of"], "note19a": ["Ezek. 12:9", "37:18"], "note1a": ["2Kgs. 25:1", "Jer. 39:1", "52:4", "Ezek. 1:2", "33:21"], "note7a": ["Lev. 17:13"], "note3b": ["Jer. 1:13 (1314)"], "note3a": ["Ezek. 44:6 (67)"], "note17d": ["Lev. 13:45"], "note17c": ["Lev. 10:6"], "note17b": ["Ex. 33:4"], "note17a": ["Jer. 16:5 (5, 7)"], "note13c": ["Ezek. 5:13"], "note13b": ["Jer. 13:27", "D&C 128:24"], "note13a": ["TGFilthiness"], "note26a": ["Ezek. 33:21 (2122)"], "note24a": ["Ezek. 12:6"], "note24b": ["Ezek. 12:11"], "note2a": ["Jer. 39:1"], "note2b": ["1Ne. 10:3"], "note14a": ["2Ne. 9:17"]}

5.HTML

{"note11d": ["Ezek. 7:4", "20:17"], "note11a": ["TGSacrilege"], "note11c": ["Deut. 8:19 (1820)"], "note11b": ["TGIdolatry"], "note8a": ["D&C 42:91"], "note6a": ["Alma 24:30"], "note6b": ["1Ne. 1:19 (1820)", "2:13", "7:14"], "note7a": ["Ezek. 11:12"], "note15a": ["Jer. 24:9"], "note17b": ["Ezek. 14:21", "33:27", "Rev. 6:8"], "note17a": ["Ezek. 36:13 (815)"], "note13c": ["D&C 20:36"], "note13b": ["Ezek. 21:17", "38:18"], "note13a": ["Ezek. 24:13 (1314)"], "note10b": ["TGIsrael, Scattering of"], "note10a": ["Jer. 19:9", "Lam. 2:20"], "note9a": ["Dan. 9:12"], "note9b": ["D&C 29:21"], "note5a": ["1Ne. 21:6", "Alma 4:11", "39:11", "D&C 103:9 (89)"], "note2a": ["Ezek. 12:14"], "note14b": ["Ezek. 22:4"], "note14c": ["Lam. 2:15"], "note14a": ["Lev. 26:31", "Neh. 2:17"]}

29.HTML

{"note3a": ["2Ne. 8:9"], "note2a": ["Isa. 19:1", "Jer. 25:19", "Joel 3:19"], "note6a": ["2Kgs. 18:21", "Isa. 36:6"], "note12a": ["Jer. 46:19", "Ezek. 30:23 (23, 26)"], "note13a": ["Jer. 46:26"], "note18a": ["Ezek. 26:7"], "note4a": ["Ezek. 38:4"]}

45.HTML

{"note11a": ["Isa. 5:10 (810)"], "note11b": ["Lev. 27:16"], "note8b": ["Ezek. 46:18"], "note8c": ["TGKings, Earthly"], "note23a": ["Num. 28:15"], "note8d": ["TGIsrael, Twelve Tribes of"], "note21b": ["TGBread, Unleavened"], "note21a": ["Lev. 23:5"], "note6a": ["Ezek. 42:20", "48:15"], "note25a": ["Lev. 23:34"], "note4b": ["Ezek. 40:46"], "note4a": ["Ezek. 48:11 (1112)"], "note1a": ["Ezek. 40:3"], "note1b": ["Rev. 11:1"], "note7a": ["Ezek. 44:3", "48:21"], "note3b": ["Ezek. 41:4", "44:13"], "note3a": ["Ezek. 48:10"], "note24a": ["Ex. 29:40"], "note20a": ["Lev. 4:27"], "note17d": ["Lev. 14:19"], "note17c": ["TGSolemn Assembly"], "note17b": ["Num. 28:11", "Ezek. 46:1"], "note17a": ["Ex. 29:40"], "note13a": ["TGSacrifice"], "note10a": ["Lev. 19:36", "Deut. 25:15 (1415)", "Amos 8:5"], "note9a": ["TGJustice"], "note20b": ["TGReconciliation"], "note5a": ["Ezek. 48:13"], "note5b": ["Ezek. 44:11", "46:24"], "note2a": ["Ezek. 42:20"], "note2b": ["Ezek. 48:15"], "note12a": ["Gen. 23:15", "Ex. 30:13"]}

14.HTML

{"note1a": ["Ezek. 8:1"], "note10a": ["TGPunish"], "note11a": ["TGIsrael, Mission of"], "note3c": ["2Kgs. 3:13", "2Cor. 6:14 (1416)"], "note3b": ["Ezek. 20:3", "36:37"], "note3a": ["TGWickedness"], "note8a": ["Lev. 17:10"], "note22a": ["TGIsrael, Remnant of"], "note8b": ["Num. 26:10"], "note17a": ["Lev. 26:25"], "note21a": ["Ezek. 5:17", "33:27", "Rev. 6:8"], "note14d": ["Job 1:1"], "note13a": ["Lev. 26:26"], "note14a": ["Jer. 15:1"], "note14b": ["Gen. 6:9"], "note14c": ["Ezek. 28:3", "Dan. 9:22 (2223)"], "note4a": ["Ezek. 3:20", "2Ne. 26:20"]}

37.HTML

{"note11a": ["Isa. 49:14"], "note23a": ["Ezek. 36:25"], "note23b": ["Zech. 9:16"], "note23c": ["TGPurification"], "note27a": ["D&C 124:38 (3740)"], "note27b": ["Ezek. 34:24 (2031)"], "note21b": ["TGIsrael, Gathering of"], "note25b": ["Isa. 60:21"], "note25c": ["Ezek. 34:24", "44:3"], "note25a": ["Ezek. 28:25", "36:28"], "note19a": ["TGBook of Mormon"], "note1a": ["Ezek. 1:3"], "note28a": ["TGHeathen"], "note1c": ["TGGod, Spirit of"], "note1b": ["1Kgs. 18:12", "Luke 4:1"], "note7a": ["D&C 138:17 (1117, 43)"], "note24a": ["Jer. 30:9", "Ezek. 34:23"], "note28c": ["TGSanctification"], "note17a": ["1Ne. 13:41", "2Ne. 3:12"], "note10a": ["TGBreath of Life"], "note22d": ["1Kgs. 12:1620"], "note9b": ["Dan. 11:4", "Rev. 7:1"], "note22c": ["Ezek. 34:23"], "note22b": ["TGIsrael, Restoration of"], "note22a": ["Jer. 50:4", "John 10:16", "TGUnity"], "note26c": ["TGRestoration of the Gospel"], "note26b": ["TGNew and Everlasting Covenant"], "note26a": ["Ezek. 34:25"], "note26d": ["TGGod, Presence of", "Temple"], "note18a": ["Ezek. 12:9", "24:19"], "note5a": ["TGBreath of Life"], "note24b": ["TGShepherd"], "note16d": ["TGScriptures, Writing of"], "note16e": ["TGIsrael, Joseph, People of"], "note16f": ["D&C 27:5"], "note28b": ["TGGod, Knowledge about"], "note16a": ["Num. 17:2 (110)", "TGScriptures to Come Forth"], "note16b": ["TGScriptures, Preservation of"], "note16c": ["TGIsrael, Judah, People of"], "note12a": ["TGJesus Christ, Prophecies about"], "note12b": ["TGResurrection"], "note12c": ["TGIsrael, Gathering of"], "note12d": ["Ezek. 36:28 (2428)"], "note14a": ["Alma 40:23 (1624)", "D&C 88:15 (1517)"]}

46.HTML

{"note1a": ["1Chr. 9:18"], "note1c": ["TGSabbath"], "note18a": ["Ezek. 45:8"], "note11a": ["TGSolemn Assembly"], "note1d": ["Ezek. 45:17"], "note9a": ["Ex. 23:14 (117)", "Deut. 16:16"], "note24a": ["Ezek. 44:11", "45:5"], "note20b": ["Lev. 2:4"], "note2a": ["Ezek. 41:21"], "note2b": ["Ezek. 40:38"], "note20a": ["Ezek. 40:39"], "note17a": ["TGLiberty"], "note18b": ["Mosiah 2:14 (1215)"], "note12a": ["D&C 58:26 (2629)"], "note12b": ["TGSacrifice"], "note12c": ["Lev. 7:16", "22:23"], "note19a": ["Ezek. 42:4 (412)"], "note13a": ["Ex. 29:38"], "note4a": ["Num. 28:10 (910)"]}

8.HTML

{"note1a": ["Ezek. 14:1", "TGElder"], "note1b": ["Ezek. 1:3"], "note11a": ["TGSeventy"], "note3c": ["Ezek. 1:1", "40:2"], "note3b": ["TGHeaven"], "note3a": ["TGGod, Spirit of"], "note2a": ["Ezek. 1:27 (2627)"], "note16d": ["TGIdolatry"], "note9a": ["TGSuperstitions"], "note16a": ["1Kgs. 6:36"], "note16b": ["1Kgs. 6:3"], "note16c": ["Jer. 2:27", "32:33"], "note4b": ["Ezek. 3:22"], "note4a": ["TGGod, Glory of"]}

7.HTML

{"note11a": ["TGMourning"], "note8a": ["Ezek. 20:8"], "note23a": ["TGBlood, Shedding of"], "note27a": ["TGGod, Knowledge about"], "note21a": ["Dan. 8:11", "11:31"], "note19a": ["Zeph. 1:3", "1Ne. 14:1"], "note4c": ["TGGod, Knowledge about"], "note19b": ["TGWickedness"], "note4b": ["Ezek. 9:10", "11:21", "16:43", "22:31", "D&C 1:10"], "note3a": ["1Sam. 3:13", "Ezek. 18:30"], "note15a": ["Deut. 32:25", "Lam. 1:20"], "note17a": ["Ezek. 21:7"], "note4a": ["Ezek. 5:11"], "note20a": ["TGIdolatry"], "note22b": ["Jer. 7:14", "Ezek. 24:21"], "note22a": ["TGPollution"], "note26b": ["Mal. 2:7 (19)"], "note26a": ["Jer. 4:20"], "note18b": ["TGShame"], "note18c": ["Isa. 3:24 (126)", "22:12"], "note18a": ["Lam. 2:10"], "note2a": ["Lam. 4:18", "Amos 8:2"], "note16a": ["Jer. 44:28", "Ezek. 6:8"], "note16b": ["TGMourning"], "note12a": ["Isa. 24:2 (13)"]}

41.HTML

{"note21a": ["Ezek. 46:2"], "note10a": ["Ezek. 40:17", "42:4 (1, 45)"], "note8a": ["Ezek. 40:5", "43:13"], "note2a": ["Ezek. 47:1"], "note2b": ["1Kgs. 6:2"], "note22b": ["Ezek. 44:16", "Mal. 1:12 (7, 12)"], "note23a": ["1Kgs. 6:31 (3133)"], "note25a": ["1Kgs. 7:6"], "note16a": ["Ezek. 42:3"], "note18c": ["Ezek. 40:16"], "note18b": ["Rev. 7:9"], "note6a": ["1Kgs. 6:5 (5, 8)"], "note13a": ["Ezek. 40:47", "42:8"], "note18a": ["1Kgs. 6:29 (29, 32, 35)", "7:36"], "note22a": ["Ex. 30:1", "Rev. 11:1"], "note18d": ["Ezek. 10:14"], "note4a": ["1Kgs. 6:16", "Ezek. 44:13", "45:3", "D&C 124:39"]}

25.HTML

{"note9b": ["1Chr. 5:8"], "note8b": ["Ezek. 35:2 (115)"], "note8c": ["TGHeathen"], "note8a": ["Isa. 15:1"], "note2a": ["Gen. 19:38 (3638)"], "note15a": ["Isa. 14:31 (2932)", "Jer. 47:1", "Amos 1:8"], "note9a": ["Josh. 12:3"], "note16a": ["Zeph. 2:5 (45)"], "note13b": ["Mal. 1:3"], "note12a": ["Ps. 137:7"], "note13a": ["Jer. 49:7"], "note5a": ["Jer. 49:2", "Ezek. 21:20", "Amos 1:14"], "note14a": ["Isa. 34:5", "Jer. 49:17 (722)", "Amos 9:12", "Obad. 1:18 (8, 1819, 21)"]}

10.HTML

{"note1a": ["Ezek. 1:22"], "note1b": ["Ezek. 1:26"], "note11a": ["Ezek. 1:17"], "note11c": ["Ezek. 1:9"], "note9a": ["Ezek. 1:16"], "note2a": ["Rev. 8:5"], "note18a": ["TGGod, Manifestations of"], "note19b": ["Ezek. 11:1"], "note20a": ["Ezek. 1:22"], "note15b": ["Ezek. 1:1"], "note17a": ["Ezek. 1:20"], "note18b": ["Hosea 9:12"], "note14a": ["Ezek. 41:18 (1819)"], "note19a": ["TGCherubim"], "note4a": ["TGGod, Manifestations of"]}

13.HTML

{"note10b": ["Ezek. 22:28"], "note10a": ["Jer. 28:9 (89)", "TGPeace"], "note8b": ["TGHonesty"], "note3a": ["Hosea 9:7 (79)", "2Ne. 28:9", "Hel. 13:29"], "note9b": ["Ezek. 20:38"], "note2a": ["TGFalse Prophets"], "note22a": ["TGFalse Doctrine", "Lying"], "note23a": ["Micah 3:6"], "note9a": ["TGBook of Life", "Book of Remembrance", "Record Keeping"], "note23b": ["TGSorcery"], "note16a": ["TGPeace"], "note6a": ["TGSuperstitions"], "note13a": ["Rev. 11:19", "16:21", "Mosiah 12:6", "D&C 29:16"], "note19a": ["Prov. 28:21"]}

26.HTML

{"note1a": ["Ezek. 1:2", "33:21"], "note18a": ["Ezek. 26:15"], "note7b": ["Jer. 1:14"], "note7a": ["Ezek. 29:18"], "note3a": ["Jer. 34:1"], "note2a": ["Isa. 23:1", "Amos 1:9"], "note2b": ["TGJerusalem"], "note15a": ["Ezek. 26:18", "39:6", "2Ne. 10:20"], "note15b": ["Jer. 49:21"], "note16a": ["Isa. 23:8"], "note17a": ["Isa. 23:4"], "note6a": ["1Ne. 21:26 (2526)", "Mosiah 11:22 (2022)", "D&C 43:25"], "note19b": ["Ezek. 27:34", "3Ne. 9:7 (78)"], "note14a": ["Ezek. 26:4"], "note19a": ["3Ne. 10:7 (78)"], "note4a": ["Ezek. 26:14"]}

9.HTML

{"note10a": ["Ezek. 7:4"], "note9a": ["Isa. 47:10"], "note2e": ["Ex. 27:1 (18)"], "note3a": ["TGCherubim"], "note2a": ["2Kgs. 15:35", "2Chr. 27:3"], "note2b": ["Dan. 12:6"], "note2c": ["TGScriptures, Writing of"], "note6a": ["Jer. 25:29"], "note4a": ["D&C 77:9"]}

47.HTML

{"note1a": ["Ezek. 41:2 (12)"], "note1c": ["TGTemple"], "note1b": ["Joel 3:18", "Zech. 14:8", "Rev. 22:1"], "note8b": ["2Kgs. 2:21"], "note19a": ["Num. 20:13", "Deut. 33:8", "Ps. 106:32"], "note3a": ["Ezek. 40:3", "Zech. 2:1"], "note8a": ["Deut. 3:17"], "note2a": ["Ezek. 40:6"], "note22a": ["Josh. 1:6"], "note22b": ["Ex. 12:48 (19, 4849)", "Lev. 16:29"], "note13b": ["Josh. 14:4", "Ezek. 48:4 (45)"], "note12a": ["Rev. 22:2"], "note12b": ["Alma 46:40"], "note18a": ["Joel 2:20"], "note14b": ["Num. 34:2 (215)", "Ezek. 48:29 (129)", "2Ne. 10:7 (78)"], "note13a": ["TGIsrael, Twelve Tribes of"], "note14a": ["Ps. 119:48 (4448)"]}

3.HTML

{"note23a": ["Ezek. 1:28"], "note27a": ["TGProphecy", "Teaching with the Spirit"], "note27b": ["TGProphets, Mission of"], "note21a": ["D&C 1:4"], "note6a": ["Matt. 11:21 (21, 23)"], "note19a": ["TGProphets, Mission of", "Warn"], "note7a": ["TGHardheartedness", "Stubbornness"], "note3a": ["Jer. 15:16", "Ezek. 2:8", "Rev. 10:10 (910)"], "note17b": ["TGPriesthood, Magnifying Callings within"], "note17a": ["TGDelegation of Responsibility", "Watchman"], "note20a": ["Ezek. 18:24"], "note20c": ["TGRighteousness"], "note20b": ["Ezek. 33:12 (1213, 18)"], "note20d": ["Jer. 6:21", "Ezek. 14:4 (37)", "2Ne. 26:20"], "note22a": ["Ezek. 8:4"], "note26a": ["Ezek. 24:27"], "note18b": ["TGWatch"], "note18c": ["TGJustice"], "note18a": ["Ezek. 33:14"], "note18d": ["TGSin"], "note18e": ["TGAccountability"], "note12a": ["TGGod, Spirit of", "Guidance, Divine"]}

39.HTML

{"note23a": ["TGConversion"], "note23b": ["Deut. 31:17 (1617)"], "note23c": ["Lev. 26:25"], "note27a": ["TGIsrael, Gathering of"], "note21a": ["TGConversion", "Heathen"], "note25b": ["TGIsrael, Twelve Tribes of"], "note6a": ["Ezek. 38:22"], "note6c": ["Ezek. 26:15", "27:15 (3, 67, 15)", "2Ne. 10:20"], "note1a": ["Ezek. 38:2 (23)"], "note7d": ["Ezek. 38:23"], "note7c": ["TGConversion"], "note7b": ["Ezek. 20:39", "43:7", "TGPollution"], "note7a": ["TGGod, Knowledge about"], "note3a": ["Joel 2:20", "Zech. 12:9"], "note17a": ["D&C 29:20 (1720)"], "note29b": ["Zech. 12:10"], "note29a": ["Isa. 54:8"], "note10a": ["Isa. 14:2"], "note24a": ["Ezek. 36:19"], "note24c": ["TGTransgress"], "note24b": ["TGUncleanness"], "note2a": ["Ezek. 38:15"]}

