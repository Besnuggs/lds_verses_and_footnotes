from bs4 import BeautifulSoup
import json
from collections import defaultdict
import os
import re

path = "./"
# Read every file in directory
dirpath = os.getcwd()
foldername = os.path.basename(dirpath)

# for filename in os.listdir(path):

for (dirname, dirs, files) in os.walk('.'):
	f = open("verseNotes.txt", "a")
	f.write("**" * 20 + " " + dirname + " " + "**" * 20 + "\n")
	for filename in files:
		if filename.endswith('.html') :
			with open(dirname + "/" + filename, "r") as f:
				parsed_html = BeautifulSoup(f, "lxml")
				tags = parsed_html.find_all('a', attrs={'class':'scripture-ref'})

				data = []

				# markers = parsed_html.find_all(attrs={"data-marker":True})

				markers = parsed_html.find_all(attrs={"data-marker": re.compile("\D")})

				# markers = parsed_html.find_all(attrs={"data-marker": "a"})

				for m in markers:
					tags = m.find_all('a', attrs={'class':'scripture-ref'})

					for t in tags:
						parent = t.find_parent('li')
						parent_id = parent['id']
						
						data.append([parent_id, t.text.encode('ascii', 'ignore').decode('ascii').encode("utf-8")])

				# print(data)

				d = defaultdict(list) 

				for key, value in data:
					d[key].append(value)

				# print(d)
				d = json.dumps(d)
				print(d)

				f = open("notes.txt", "a")
				# f.write(dirname + " - ")
				f.write(filename.upper() + "\n")
				f.write("\n")
				f.write(d + "\n")
				f.write("\n")
				
				f.close()
