**************************************** . ****************************************
1.HTML

{"note1a": ["Eccl. 7:27"], "note18a": ["TGKnowledge"], "note11a": ["TGVeil"], "note9a": ["Eccl. 3:15"], "note3a": ["1Sam. 12:21", "Matt. 16:26", "2Ne. 9:51"], "note8a": ["Prov. 27:20"], "note2a": ["TGVanity"], "note15a": ["Eccl. 7:13"], "note17a": ["Eccl. 7:25", "Mosiah 12:27"], "note18b": ["TGSorrow"], "note13b": ["Eccl. 4:8"], "note13a": ["D&C 97:1 (12)"], "note14a": ["TGVanity"], "note14b": ["Eccl. 2:17 (17, 26)", "4:4 (4, 6, 16)", "6:9"], "note4a": ["TGEarth, Destiny of"]}

2.HTML

{"note1a": ["Prov. 14:13", "D&C 88:121"], "note1b": ["TGPleasure", "Selfishness"], "note17a": ["Eccl. 1:14"], "note8b": ["1Kgs. 20:14"], "note24a": ["Luke 12:19 (1921)"], "note24b": ["TGLabor"], "note8a": ["1Kgs. 9:28", "10:10 (1014)"], "note15a": ["Eccl. 6:11"], "note20a": ["TGDespair"], "note26a": ["Prov. 13:22"], "note10a": ["Eccl. 3:22", "5:18", "9:9"], "note21b": ["TGVanity"], "note16a": ["Eccl. 3:19"], "note16b": ["Ps. 49:10"], "note13a": ["TGDarkness, Spiritual"], "note14a": ["Prov. 17:24"], "note14b": ["Eccl. 9:2"], "note4a": ["1Kgs. 7:1 (112)"]}

12.HTML

{"note1a": ["Alma 37:35"], "note1b": ["Eccl. 11:8"], "note7c": ["TGMan, a Spirit Child of Heavenly Father"], "note7b": ["TGDeath", "Man, Antemortal Existence of", "Spirit Body", "Spirit Creation", "Spirits, Disembodied"], "note7a": ["TGMan, Physical Creation of", "Mortality"], "note14b": ["TGJesus Christ, Judge", "Judgment, the Last"], "note13b": ["TGObedience"], "note13c": ["TGDuty", "Mission of Early Saints"], "note12a": ["TGStudy"], "note13a": ["TGReverence"], "note5a": ["Job 17:13", "Alma 40:11"], "note14a": ["TGGood Works"]}

4.HTML

{"note7a": ["TGVanity"], "note8b": ["Eccl. 1:13"], "note4b": ["Eccl. 1:14"], "note9b": ["TGReward"], "note9a": ["Moses 3:24"], "note13b": ["TGTeachable", "Warn"], "note6a": ["Prov. 15:16"], "note6b": ["TGSilence"], "note5a": ["2Ne. 9:28"], "note13a": ["3Ne. 12:3"], "note4a": ["Gen. 26:14"]}

6.HTML

{"note10a": ["Job 9:3 (14)", "Isa. 45:9"], "note11a": ["TGVanity"], "note11b": ["Eccl. 2:15"], "note9a": ["TGSight"], "note3a": ["Isa. 14:20", "Jer. 22:19"], "note9b": ["TGVanity"], "note9c": ["Eccl. 1:14"]}

11.HTML

{"note1a": ["TGBread"], "note10b": ["TGVanity"], "note9b": ["Job 31:7"], "note7a": ["TGLight [noun]"], "note8b": ["TGVanity"], "note9c": ["TGJudgment, the Last"], "note8a": ["Eccl. 12:1"], "note2a": ["D&C 39:21"], "note9a": ["TGHappiness"], "note10a": ["TGCarnal Mind"], "note4a": ["TGHarvest"], "note5a": ["John 3:8 (58)"], "note5b": ["TGCreation"]}

5.HTML

{"note1a": ["TGReverence"], "note10b": ["TGVanity"], "note10a": ["TGTreasure"], "note7b": ["Eccl. 3:14"], "note7a": ["TGVanity"], "note2d": ["Matt. 6:7"], "note19a": ["TGTreasure"], "note8a": ["TGOppression"], "note2a": ["TGRashness"], "note2b": ["TGHaste"], "note2c": ["Matt. 12:36", "Eph. 5:4"], "note15a": ["Job 1:21"], "note8b": ["TGJudgment"], "note19b": ["TGLabor"], "note18b": ["Eccl. 2:10"], "note12a": ["TGSleep"], "note12b": ["TGIndustry", "Labor"], "note18a": ["TGIndustry"], "note5a": ["TGHonesty"], "note19c": ["TGGod, Gifts of"], "note4a": ["Lev. 22:21"]}

8.HTML

{"note1a": ["D&C 50:24", "84:45"], "note11a": ["D&C 98:2"], "note11b": ["Ex. 8:15 (1315)"], "note8b": ["TGDeliver"], "note14b": ["Eccl. 7:15", "Mal. 3:14 (1418)"], "note8a": ["2Ne. 9:6"], "note2a": ["TGCitizenship", "Governments"], "note17b": ["2Ne. 9:28", "D&C 76:9"], "note16a": ["D&C 1:26", "42:68"], "note17a": ["Eccl. 3:11"], "note12a": ["Ex. 1:20 (720)", "D&C 1:9"], "note12b": ["Isa. 3:10", "TGReverence"], "note14a": ["TGVanity"], "note5a": ["1Ne. 20:18"], "note13a": ["Jer. 44:10 (1011)", "D&C 10:56"], "note5b": ["TGTime"]}

7.HTML

{"note8a": ["TGPatience"], "note27a": ["Eccl. 1:1"], "note25b": ["D&C 42:68"], "note6a": ["TGLaughter", "Levity"], "note6b": ["TGVanity"], "note25a": ["Eccl. 1:17", "Mosiah 12:27"], "note19a": ["Prov. 21:22", "Eccl. 9:16 (1618)", "D&C 52:17"], "note19b": ["Prov. 24:5"], "note1a": ["Prov. 22:1"], "note3a": ["2Cor. 7:10"], "note15a": ["TGVanity"], "note20a": ["D&C 33:4"], "note15b": ["Eccl. 8:14"], "note13a": ["Eccl. 1:15"], "note20b": ["TGSin"], "note9a": ["TGRashness"], "note9c": ["TGAnger"], "note9b": ["Prov. 29:22 (2123)"], "note26c": ["1Ne. 22:15 (1517)"], "note26b": ["TGWoman"], "note26a": ["Prov. 5:4"], "note26d": ["Prov. 2:19"], "note5a": ["TGReproof"], "note2a": ["TGMourning"], "note12a": ["D&C 6:7"], "note12c": ["Prov. 3:18 (1319)"], "note14a": ["TGAdversity"]}

10.HTML

{"note1b": ["Josh. 7:11 (1, 1112)", "Eccl. 9:18"], "note7a": ["Prov. 19:10"], "note20a": ["TGCitizenship", "Kings, Earthly"], "note8a": ["Alma 30:60"], "note16a": ["Isa. 3:4 (45, 12)"], "note17a": ["TGWord of Wisdom"], "note18b": ["TGIdleness"], "note12a": ["Prov. 10:32"], "note12b": ["2Ne. 9:28 (2829)", "19:17", "D&C 35:7"], "note18a": ["TGLaziness"]}

9.HTML

{"note10b": ["2Ne. 9:13 (1314)", "D&C 130:18"], "note10c": ["Alma 34:33"], "note9b": ["TGMarriage, Husbands", "Marriage, Wives"], "note11a": ["Amos 2:14 (1415)", "Mosiah 4:27"], "note7a": ["TGCheerful"], "note9a": ["TGJoy"], "note3a": ["Alma 40:11 (1112)"], "note8a": ["Alma 5:21 (21, 24, 27)"], "note9e": ["Eccl. 2:10"], "note2a": ["Eccl. 2:14"], "note2b": ["Alma 12:8"], "note9d": ["TGVanity"], "note10a": ["TGIndustry"], "note16a": ["Eccl. 7:19"], "note16b": ["Mark 6:2 (23)"], "note18b": ["Josh. 7:11 (1, 1112)", "Eccl. 10:1"], "note12a": ["Prov. 7:23 (2123)"], "note18a": ["D&C 6:7"], "note9c": ["TGFamily, Love within"], "note4a": ["TGHope"]}

3.HTML

{"note11a": ["TGBeauty"], "note11c": ["Eccl. 8:17"], "note8a": ["TGHate"], "note4b": ["TGMourning"], "note4a": ["TGLaughter"], "note1a": ["Prov. 15:23"], "note1b": ["TGTime"], "note7b": ["TGSilence"], "note7a": ["Gen. 44:13"], "note19a": ["Eccl. 2:16"], "note15a": ["Eccl. 1:9"], "note20a": ["Gen. 3:19"], "note17a": ["TGJesus Christ, Judge", "Judgment, the Last"], "note13a": ["TGGod, Gifts of"], "note10b": ["D&C 122:7 (57)"], "note9a": ["Alma 29:15 (1416)"], "note22a": ["Eccl. 2:10"], "note18b": ["Ps. 73:22"], "note2a": ["Acts 17:26", "Alma 40:10", "D&C 138:53 (5356)"], "note2b": ["2Kgs. 20:1 (16)", "D&C 42:48"], "note16a": ["4Ne. 1:44"], "note12a": ["Ps. 34:14"], "note14b": ["1Ne. 10:18 (1819)"], "note14c": ["Eccl. 5:7", "Heb. 12:28", "Mosiah 4:1"], "note14a": ["TGMarriage, Celestial"]}

