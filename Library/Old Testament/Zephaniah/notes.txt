**************************************** . ****************************************
1.HTML

{"note1a": ["2Kgs. 21:24"], "note10a": ["2Chr. 33:14"], "note7b": ["TGDay of the Lord"], "note7a": ["Hab. 2:20"], "note13b": ["Deut. 28:30 (30, 39)"], "note3a": ["Ezek. 7:19"], "note8a": ["Morm. 8:36 (3637)"], "note2a": ["1Ne. 22:23", "2Ne. 26:6", "D&C 101:24"], "note18a": ["Joel 1:19 (1920)"], "note14a": ["Mal. 4:5", "D&C 110:16"], "note15a": ["TGWorld, End of"], "note16b": ["D&C 101:57"], "note12c": ["Mal. 3:14"], "note6a": ["Jer. 11:10 (910)"], "note12b": ["Jer. 48:11"], "note4a": ["2Ne. 19:21"], "note5a": ["2Kgs. 23:12", "Jer. 19:13"], "note13a": ["Amos 5:11"], "note5b": ["1Kgs. 11:33"]}

2.HTML

{"note11a": ["D&C 45:74"], "note11c": ["TGHeathen"], "note7a": ["Deut. 30:3 (15)", "Ps. 14:7", "Amos 9:14 (1415)", "Zeph. 3:20"], "note11b": ["Mal. 1:11"], "note3c": ["TGWorld, End of"], "note3b": ["TGRighteousness"], "note3a": ["Ps. 76:9", "3Ne. 12:5", "D&C 88:17 (1517)"], "note8a": ["TGReviling"], "note14a": ["Isa. 34:11 (1011)"], "note15a": ["Jer. 18:16"], "note8b": ["Jer. 48:42 (29, 42)"], "note9a": ["Gen. 19:24"], "note12a": ["Ezek. 30:5 (45)"], "note13a": ["2Ne. 20:12 (12, 2425)"], "note4a": ["Zech. 9:5"], "note5a": ["Ezek. 25:16"], "note5b": ["Obad. 1:19"]}

3.HTML

{"note1a": ["TGFilthiness"], "note9b": ["TGCommon Consent", "Unity"], "note11a": ["TGHaughtiness"], "note8b": ["Isa. 13:4"], "note8c": ["TGEarth, Destiny of", "World, End of"], "note3a": ["Jer. 5:6"], "note8a": ["Joel 3:2 (12)"], "note2a": ["TGDisobedience"], "note20b": ["Isa. 62:7"], "note15a": ["TGJesus Christ, Millennial Reign"], "note9a": ["TGCommunication", "Language"], "note20a": ["Isa. 63:4"], "note10a": ["Isa. 18:1"], "note13a": ["TGLying"], "note4b": ["TGSacrilege"], "note20c": ["Deut. 30:3 (15)", "Zeph. 2:7"], "note19b": ["TGIsrael, Gathering of"]}

