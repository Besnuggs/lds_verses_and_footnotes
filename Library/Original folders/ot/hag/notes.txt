**************************************** . ****************************************
1.HTML

{"note1c": ["1Chr. 3:19 (1719)", "Ezra 2:2 (12)", "Matt. 1:12 (1113)"], "note1b": ["Hag. 2:1, 10 (10, 20)"], "note11a": ["TGDrought", "Famine"], "note7b": ["2Pet. 2:15"], "note7a": ["TGMeditation"], "note6d": ["TGWages"], "note8a": ["Ezra 6:3", "D&C 88:119", "95:3"], "note10a": ["Deut. 28:18"], "note12a": ["TGPriesthood, Authority"], "note6a": ["Deut. 28:38 (3840)", "Micah 6:15"], "note6b": ["Isa. 9:20"], "note6c": ["TGFamily, Managing Finances in"], "note5a": ["Hag. 2:18"], "note6e": ["TGWaste"]}

2.HTML

{"note1a": ["Hag. 1:1"], "note10a": ["Hag. 1:1"], "note7b": ["Ex. 40:34", "Isa. 60:7", "D&C 97:15"], "note7a": ["TGJesus Christ, Second Coming"], "note9a": ["TGPeace", "Peace of God"], "note3a": ["Ezra 3:12 (1113)"], "note8a": ["D&C 38:39"], "note22c": ["Zech. 14:13"], "note22b": ["Micah 5:10"], "note22a": ["Dan. 2:44"], "note17c": ["Jer. 5:3", "Amos 4:6 (611)"], "note23b": ["Ps. 89:3", "Isa. 43:10"], "note17a": ["Deut. 28:22"], "note21a": ["TGEarth, Destiny of"], "note6a": ["Isa. 13:13", "Ezek. 38:19 (1920)", "TGLast Days"], "note18a": ["Hag. 1:5"], "note5a": ["Ex. 29:45 (41, 45)"], "note4a": ["D&C 27:15 (1518)", "75:22"]}

