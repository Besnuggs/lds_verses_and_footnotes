const fs = require('fs'),
    cheerio = require('cheerio'),
    axios = require('axios'),
    bookOfMormonPath = ('Book of Mormon/'),
    oldTestamentPath = ('Old Testament/'),
    newTestamentPath = ('New Testament/'),
    doctrineAndCovenants = ('Doctrine and Covenants/'),
    pearlOfGreatPrice = ('Pearl of Great Price/'),
    otherText = ('OtherTexts/'),
    creationText = "DROP TABLE IF EXISTS public.lds_verses_and_footnotes; \n" +
    "CREATE TABLE IF NOT EXISTS public.lds_verses_and_footnotes ( \n" +
    "number INTEGER, \n" +
    "text TEXT, \n" +
    "chapter INTEGER, \n" +
    "document TEXT, \n" +
    "footnote_keywords TEXT, \n" +
    "footnote_indices TEXT, \n" +
    "footnote_references TEXT, \n" +
    "id INTEGER NOT NULL UNIQUE \n" +
        "); \n",
    booksInOrder = require('./bookOrder.js').booksInOrder,
    lastInsertions = require('./lastInsertions.js').lastInsertions
    

//**************************************************************************** 
// fs dynamic variables
let path,
booksArray = [],
bookName;

/* All Verses Are Eventually Put In This Array */
// *************************
let allVerses = [];
// *************************

function createSQLFile(){
    fs.writeFile('lds_verses_and_footnotes.sql', 1, (error) => {
        if(error) throw error;
        console.log('God has answered my prayer.')
    }, () => {
        fs.writeFile('./lds_verses_and_footnotes.sql', creationText, (error) => {
            if (error) throw error;
            console.log("I've written on sql file")
            TestamentCreation(oldTestamentPath);
        })
    });
}

function sortNumber(a, b) {
    a = a.replace(/\D/g,'');
    b = b.replace(/\D/g,'');
    return a - b;
  }

function ScrapeVolume(volumeData){
// *Test Path *//
const $ = cheerio.load(fs.readFileSync(volumeData[0].path + volumeData[0].chapterFiles[0]),'utf-8')
const markerMath = [
    {marker: "a",
    value: 0},
    {marker: "b",
    value: -1},
    {marker: "c",
    value: -2},
    {marker: "d",
    value: -3},
    {marker: "e",
    value: -4},
    {marker: "f",
    value: -5},
    {marker: "g",
    value: -6},
    {marker: "h",
    value: -7},
    {marker: "i",
    value: -8},
    {marker: "j",
    value: -9},
    {marker: "k",
    value: -10},
    {marker: "l",
    value: -11},
    {marker: "m",
    value: -12},
    {marker: "n",
    value: -13},
    {marker: "o",
    value: -14},
    {marker: "p",
    value: -15},
    {marker: "q",
    value: -16},
    {marker: "r",
    value: -17},
    {marker: "s",
    value: -18},
    {marker: "t",
    value: -19},
    {marker: "u",
    value: -20},
    {marker: "v",
    value: -21},
    {marker: "w",
    value: -22},
    {marker: "x",
    value: -23},
    {marker: "y",
    value: -24},
    {marker: "z",
    value: -25}
]

// Number - Verse number contained within chapter. This number is relative to the chapter.
function getNumbers($){
    const number = $('.verse').toArray().map((x) => {
       return $(x).find('.verse-number').text()
    })
    
    return number 
}

// Document - footnote keyword and index within string.
function getDocument($){
// Verse Text - To loop through for keywords. Keywords are pulled from keywords array. Return index with each grouping of keyword. For example of formatting, (string of keyword, string of colon and index): 'beginning'':3 '
const digitScan = /\d+/;
const verseText = $('.verse').toArray().map((x) => {
    $('.verse').find('.marker').remove();
    $('.verse').find('.verse-number').remove();
    const verses = $(x).text().substring(0,1) === '¶' ? $(x).text().slice(2, $(x).text().length) : $(x).text();
    return verses;
}).map((e,i) => {
    i = i + 1;
    return numberedVerses = {
        verseNumber: i,
        verseText: e
    }
});

const footnoteKeywords = $('.study-note-ref').toArray().map((x) => {
    const verseNumbers = $(x).attr('href') && 
    $(x).attr('href').match(digitScan) ? $(x).attr('href').match(digitScan)[0] : 
    $(x).html() === 'numbered' ? 8 :
    $(x).html() === 'Prophets*' && 6,
        keywords = $(x).text(),
        numberedKeywords = {
            verseNumber: Number(verseNumbers),
            keyword: keywords
        }
       
    return numberedKeywords
})

// Need to loop through each verseText, and then loop through the keywords, and find index of each footnote keyword.
let documentData = [];
verseText.forEach((e) => {
    footnoteKeywords.forEach((footnote) => {
        let keywordWithIndexObj
        if(e.verseNumber === footnote.verseNumber){
        // Need to split each string into an array, remove punctuation, and find index of each word.
        let index = e.verseText.split(' ').map((c) => c.replace(/[^A-Za-z0-9_]/g,"")).indexOf(footnote.keyword) + 1;
                keywordWithIndexObj = {
                verseNumber: e.verseNumber,
                footnoteKeyword: footnote.keyword,
                index
                }
            documentData.push(keywordWithIndexObj)
        }
    }) 
})


return documentData
}
// getDocument($);


// Text - Removing SuperScripts for verses text field in SQL
function getText($){
    $('.verse').find('.marker').remove();
    $('.verse').find('.verse-number').remove();
    const versesText = $('.verse').toArray().map((x) => {
        return $(x).text().substring(0,1) === '¶' ? $(x).text().slice(2, $(x).text().length) : $(x).text();
    });
    return versesText;
}

// Footnote_references - What the Footnotes reference at the bottom of the HTML document.
function getFootnoteReferences($){
    // Must include URL paths from hrefs.
    //  return studyNotes = $('.study-notes').children('.marker').toArray().map((x) => {
    //     const footnoteText = $(x).text(),
    //         footnoteMarkers = $(x).parent().parent().data('marker'),
    //         hrefs = $(x).parent().find('.scripture-ref').attr('href'),
    //         verseNumber = $(x).parent().parent().parent().parent().attr('data-marker'),
    //         footnoteReference = {verseNumber, footnoteMarkers, footnoteText, hrefs}
        
    //     console.log(footnoteReference)
    //     return footnoteReference
    // })

    return footnotesReferences = $('.scripture-ref').toArray().map((x) => {
    const footnoteMarkers = $(x).parent().parent()
    .data('marker'),
    hrefs = $(x).parent().find('.scripture-ref').attr('href'),
    footnoteText  = $(x).text(),
    verseNumber = $(x).parent().parent().parent().parent().attr('data-marker'),
    footnoteReference = {verseNumber, footnoteMarkers, footnoteText, hrefs};
    
    // const html = $.root().html()

    // if(!footnoteReference.verseNumber){
    //     console.log(footnoteReference, html, 'WRONG VERSENUMBER')
    // }
    
    return footnoteReference;
    })
}

function getFootnoteText($){
const digitScan = /\d+/;
const footnoteTextAndMarkers = $('.study-note-ref').toArray().map((x) => {
    const marker = $(x).find('.marker').text(),
        text = $(x).text().substr(1),
        verseNumbers = $(x).attr('href') && 
        $(x).attr('href').match(digitScan) ? $(x).attr('href').match(digitScan)[0] : 
        $(x).html() === 'numbered' ? 8 :
        $(x).html() === 'Prophets*' && 6,
        versesText = $(x).parent().toArray().map((x) => {
            // console.log($(x).attr() !== "verse")
            if($(x).attr() !== "verse"){
                const newText = $(x).closest('.verse').toArray().map((x) => {
                    const removedMarkers = $(x).clone()
                    removedMarkers.find('.verse-number').remove()
                    return $(removedMarkers).text().substring(0,1) === '¶' ? $(removedMarkers).text().slice(2, $(x).text().length) : $(removedMarkers).text();
                })
                return newText[0]
            } else {
                const newText = $(x).toArray().map((x) => {
                    const removedMarkers = $(x).clone()
                    removedMarkers.find('.verse-number').remove()
                    return $(removedMarkers).text().substring(0,1) === '¶' ? $(removedMarkers).text().slice(2, $(x).text().length) : $(removedMarkers).text();
                })
                return newText[0]
            }
        })
        
        // console.log(versesText)
        const footnoteIndex = markerMath.find((obj) => obj.marker === marker) ? versesText[0].indexOf(`${marker}${text}`) + markerMath.find((obj) => obj.marker === marker).value : null,
        footnoteTextAndMarkerObj = {
            marker: marker,
            text: text,
            verseNumber: verseNumbers,
            index: footnoteIndex
        }
        // console.log({versesText, marker, text, verseNumbers, footnoteIndex})
        function gettingText(x){
            // $('.verse').find('.marker').remove();
            // $('.verse').find('.verse-number').remove();
            const versesText = $(x).closest(".verse").toArray().map((x) => {
                const newText = $(x).toArray().map((x) => {
                    const removedMarkers = $(x).clone()
                    removedMarkers.find('.verse-number').remove()
                    return $(removedMarkers).text().substring(0,1) === '¶' ? $(removedMarkers).text().slice(2, $(x).text().length) : $(removedMarkers).text();
                })

                return newText[0]
            });

            return versesText[0]
        }
        
        return footnoteTextAndMarkerObj
})
return footnoteTextAndMarkers;
}

/**************These 'other' functions are for the remaining other works that make up the last ~60 rows *******************/
function otherNumbers($, path){
     // Title Page
    if(path.split('/')[2] === "1.html"){
        return [1,2,3]

    }
    // Introduction
    if(path.split('/')[2] === "2.html"){
        const IntroLineNumbers = $('.body-block').children('p').toArray();

       
        return IntroLineNumbers;
    }
    // Official Declaration 1
    if(path.split('/')[2] === "3.html"){


    }
    // Official Declaration 2
    if(path.split('/')[2] === "4.html"){

        
    }
}

function otherfootnoteText($, path){
     // Title Page
    if(path.split('/')[2] === "1.html"){
    const digitScan = /\d+/;
    const footnoteTextAndMarkers = $('.study-note-ref').toArray().map((x) => {
        const marker = $(x).find('.marker').text(),
        text = $(x).text().substr(1),
        verseNumbers = $(x).attr('href') && 
        $(x).attr('href').match(digitScan) ? $(x).attr('href').match(digitScan)[0] : 
        $(x).html() === 'numbered' ? 8 :
        $(x).html() === 'Prophets*' && 6,
        footnoteTextAndMarkerObj = {
            marker: marker,
            text: text,
            verseNumber: verseNumbers
        }
        
        return footnoteTextAndMarkerObj
    })
    return footnoteTextAndMarkers;

    }
    // Introduction
    if(path.split('/')[2] === "2.html"){


    }

    // Official Declaration 1
    if(path.split('/')[2] === "3.html"){


    }

    // Official Declaration 2
    if(path.split('/')[2] === "4.html"){

        
    }
}

function otherFootnoteReferences($, path){
     // Title Page
    if(path.split('/')[2] === "1.html"){
         // Must include URL paths from hrefs.
    const footnotesReferences = $('.scripture-ref').toArray().map((x) => {
        const footnoteMarkers = $(x).parent().parent()
        .data('marker'),
        hrefs = $(x).parent().find('.scripture-ref').attr('href'),
        footnoteText  = $(x).text(),
        verseNumber = $(x).parent().parent().parent().parent().attr('data-marker'),
        footnoteReference = {verseNumber, footnoteMarkers, footnoteText, hrefs};

        return footnoteReference;
    })
    
    return footnotesReferences

    }
    // Introduction
    if(path.split('/')[2] === "2.html"){


    }
    // Official Declaration 1
    if(path.split('/')[2] === "3.html"){


    }
    // Official Declaration 2
    if(path.split('/')[2] === "4.html"){

        
    }
}

function otherText($, path){
     // Title Page
    if(path.split('/')[2] === "1.html"){
        const title = $('#title1').text(),
            subtitle = $('.subtitle').text(),
            text1 = title + ' ' + subtitle,
            text2 = $('#p1').text(),
            text3 = $('#p2').text()

        return [text1, text2, text3]
    }
    // Introduction
    if(path.split('/')[2] === "2.html"){


    }
    // Official Declaration 1
    if(path.split('/')[2] === "3.html"){


    }
    // Official Declaration 2
    if(path.split('/')[2] === "4.html"){

        
    }
}

function otherDocument($, path){
     // Title Page
    if(path.split('/')[2] === "1.html"){
       const documents = $('.scripture-ref').toArray().map((x) => {
           const documentText = $(x).text(),
            documentReferences = $(x).parent().find('.scripture-ref').attr('href'),
            lineNumber = $(x).parent().data('id')

            return {documentText, documentReferences, lineNumber}
       })
       return documents
    }
    // Introduction
    if(path.split('/')[2] === "2.html"){


    }

    // Official Declaration 1
    if(path.split('/')[2] === "3.html"){


    }
    // Official Declaration 2
    if(path.split('/')[2] === "4.html"){

        
    }
}

// Chapter - Chapter number the verse is contained in. The chapter number is NOT RELATIVE TO BOOK. IT INCREMENTS LIKE IDs.
let testamentData = []
let serialChapter = 0;
let oldTestamentTotalChapters = 929,
    oldTestamentTotalVerses = 23145,
    firstBookInVolume = volumeData[0].bookName;

// Need to change serialChapter based on Volume data.
if(volumeData[0].bookName === "Matthew"){
    serialChapter = 929;
} else if(volumeData[0].bookName === "1 Nephi"){
    serialChapter = 1189;
} else if (volumeData[0].bookName === "Doctrine and Covenants"){
    serialChapter = 1428;
} else if (volumeData[0].bookName === "Moses"){
    serialChapter = 1566;
} else if (volumeData[0].bookName === "otherWork"){
    // otherWork includes Title Page, Introduction, OD1, and OD2
    serialChapter = 1582;
}



volumeData.forEach((e) => {
    e.chapterFiles.forEach((j) => {
        serialChapter++;
        const path = e.path + j,
        $ = cheerio.load(fs.readFileSync(path, 'utf-8'));
        
        const htmlObjs = {
            verseNumbers: path.split('/')[0] === 'OtherTexts' ? otherNumbers($, path) : getNumbers($),
            footnoteText: path.split('/')[0] === 'OtherTexts' ? otherfootnoteText($, path) : getFootnoteText($),
            footnoteReferences: path.split('/')[0] === 'OtherTexts' ? otherFootnoteReferences($, path) : getFootnoteReferences($),
            text: path.split('/')[0] === 'OtherTexts' ? otherText($, path) : getText($),
            chapter: serialChapter,
            document: path.split('/')[0] === 'OtherTexts' ? otherDocument($, path) : getDocument($),
        }
        testamentData.push(htmlObjs)
    })
})

appendingVerseLines(testamentData, firstBookInVolume)
}





function appendingVerseLines(volumeToWrite, firstBookInVolume){
// console.log(volumeToWrite[0], volumeToWrite[0].footnoteReferences[0], volumeToWrite[0].document)
// Need to add lines to existing sql file.  For each verse, contained within each chapter, there should be an INSERT.
// const footnoteReferences = volumeToWrite[0].footnoteReferences,
//     documentObj = volumeToWrite[0].document,
//     footnoteText = volumeToWrite[0].footnoteText

let id = 0;
// Need to change id so that verse SPK is serializing where the previous volume ends.
if (firstBookInVolume === "Matthew"){
    id = 23145;
} else if (firstBookInVolume === "1 Nephi"){
    id = 31102;
} else if (firstBookInVolume === "Doctrine and Covenants"){
    id = 37706;
} else if (firstBookInVolume === "Moses"){
    id = 41360;
} else if (firstBookInVolume === "otherWork"){
    id = 41995;
}

volumeToWrite.forEach((chapter) => {
    let chapterIndex = chapter.chapter,
     verseNumbers = chapter.verseNumbers,
     documentData = chapter.document,
     footnoteReferences = chapter.footnoteReferences,
     footnoteText = chapter.footnoteText
    
     chapter.text.forEach((verse, index) => {
        let number = verseNumbers[index],
        text = "'" + verse + "'",
        chapter = chapterIndex,
        document = organizeDocument(documentData, number) ? "'" + organizeDocument(documentData, number) + "'" : null,
        footnote = organizeFootnoteText(footnoteText, number, verse) ? "'" + organizeFootnoteText(footnoteText, number) + "'" : null,
        footnote_indices = organizeFootnoteIndices(footnoteText, number, verse) ? "'" + organizeFootnoteIndices(footnoteText, number) + "'" : null,
        footnote_references = organizeFootnoteReferences(footnoteReferences, number) ? "'" + organizeFootnoteReferences(footnoteReferences, number) + "'" : null
        id++



        let insertText = "INSERT INTO public.lds_verses_and_footnotes (number, text, chapter, document, footnote_keywords, footnote_indices, footnote_references, id) VALUES (" + number + "," + text + "," + chapter + "," + document + "," + footnote + "," + footnote_indices + "," + footnote_references + "," + id + "); \n";
        fs.appendFileSync('./lds_verses_and_footnotes.sql', insertText, (error) => {
            if (error) throw error;
            console.log("I've added to sql file")
        })
    })
})

    function organizeDocument(document, verseNumber){ 
        const relevantFootnotes = document.filter((e) => Number(e.verseNumber) === Number(verseNumber)).map((e) => {
            return "''" + e.footnoteKeyword + "'':" + e.index
        }).join(' ')
        
        return relevantFootnotes ? relevantFootnotes : null
    }
    // organizeDocument(documentObj, 1);

    function organizeFootnoteText(footNoteText, verseNumber){
        // console.log(footNoteText, verseNumber)
        const organizedFootnoteText = footNoteText.filter((e) => Number(e.verseNumber) === Number(verseNumber)),
        footnoteTextAndMarkers = organizedFootnoteText.map((footnoteTextInVerse) => {
            return `${footnoteTextInVerse.marker}. ${footnoteTextInVerse.text}`
        }).join(',;, ')

        return footnoteTextAndMarkers ? footnoteTextAndMarkers : null
    }
    // organizeFootnoteText(footnoteText, 1)

    function organizeFootnoteIndices(footnoteText, verseNumber){
        const organizedFootnoteIndices = footnoteText.filter((e) => Number(e.verseNumber) === Number(verseNumber)),
            footnoteIndicesAndMarkers = organizedFootnoteIndices.map((footnoteIndexInVerse) => {
                return `${footnoteIndexInVerse.marker}. ${footnoteIndexInVerse.index}`
            }).join(', ')

            return footnoteIndicesAndMarkers ? footnoteIndicesAndMarkers : null
    }

    function organizeFootnoteReferences(footnoteReferences, verseNumber){
        let pathRemove = "../../../scriptures/"
        let pathsToString = [
            {path: "bofm",
            string: "Book of Mormon"},
            {path: "pgp",
            string: "Pearl of Great Price"},
            {path: "dc-testament",
            string: "Doctrine and Covenants"},
            {path: "dc",
            string: "Doctrine and Covenants"},
            {path: "tg",
            string: "Topical Guide"},
            {path: "ot",
            string: "Old Testament"},
            {path: "nt",
            string: "New Testament"},
            {path: "gen",
            string: "Genesis"},
            {path: "1-chr",
            string: "1 Chronicles"},
            {path: "1-kgs",
            string: "1 Kings"},
            {path: "1-sam",
            string: "1 Samuel"},
            {path: "2-chr",
            string: "2 Chronicles"},
            {path: "2-kgs",
            string: "2 Kings"},
            {path: "2-sam",
            string: "2 Samuel"},
            {path: "amos",
            string: "Amos"},
            {path: "dan",
            string: "Daniel"},
            {path: "deut",
            string: "Deuteronomy"},
            {path: "eccl",
            string: "Ecclesiastes"},
            {path: "esth",
            string: "Esther"},
            {path: "ex",
            string: "Exodus"},
            {path: "ezek",
            string: "Ezekiel"},
            {path: "ezra",
            string: "Ezra"},
            {path: "hab",
            string: "Habakkuk"},
            {path: "hag",
            string: "Haggai"},
            {path: "hosea",
            string: "Hosea"},
            {path: "isa",
            string: "Isaiah"},
            {path: "jer",
            string: "Jeremiah"},
            {path: "job",
            string: "Job"},
            {path: "joel",
            string: "Joel"},
            {path: "jonah",
            string: "Jonah"},
            {path: "josh",
            string: "Joshua"},
            {path: "judg",
            string: "Judges"},
            {path: "lam",
            string: "Lamentations"},
            {path: "lev",
            string: "Leviticus"},
            {path: "mal",
            string: "Malachi"},
            {path: "micah",
            string: "Micah"},
            {path: "nahum",
            string: "Nahum"},
            {path: "neh",
            string: "Nehemiah"},
            {path: "num",
            string: "Numbers"},
            {path: "obad",
            string: "Obadiah"},
            {path: "prov",
            string: "Proverbs"},
            {path: "ps",
            string: "Psalms"},
            {path: "ruth",
            string: "Ruth"},
            {path: "song",
            string: "Song of Solomon"},
            {path: "zech",
            string: "Zechariah"},
            {path: "zeph",
            string: "Zephaniah"},
            {path: "1-cor",
            string: "1 Corinthians"},
            {path: "1-jn",
            string: "1 John"},
            {path: "1-pet",
            string: "1 Peter"},
            {path: "1-thes",
            string: "1 Thessalonians"},
            {path: "1-tim",
            string: "1 Timothy"},
            {path: "2-cor",
            string: "2 Corinthians"},
            {path: "2-jn",
            string: "2 John"},
            {path: "2-pet",
            string: "2 Peter"},
            {path: "2-thes",
            string: "2 Thessalonians"},
            {path: "2-tim",
            string: "2 Timothy"},
            {path: "3-jn",
            string: "3 John"},
            {path: "acts",
            string: "Acts"},
            {path: "col",
            string: "Colossians"},
            {path: "eph",
            string: "Ephesians"},
            {path: "gal",
            string: "Galatians"},
            {path: "heb",
            string: "Hebrews"},
            {path: "james",
            string: "James"},
            {path: "john",
            string: "John"},
            {path: "jude",
            string: "Jude"},
            {path: "luke",
            string: "Luke"},
            {path: "mark",
            string: "Mark"},
            {path: "matt",
            string: "Matthew"},
            {path: "philem",
            string: "Philemon"},
            {path: "philip",
            string: "Philippians"},
            {path: "rev",
            string: "Revelation"},
            {path: "rom",
            string: "Romans"},
            {path: "titus",
            string: "Titus"},
            {path: "1-ne",
            string: "1 Nephi"},
            {path: "2-ne",
            string: "2 Nephi"},
            {path: "3-ne",
            string: "3 Nephi"},
            {path: "4-ne",
            string: "4 Nephi"},
            {path: "alma",
            string: "Alma"},
            {path: "enos",
            string: "Enos"},
            {path: "ether",
            string: "Ether"},
            {path: "hel",
            string: "Helaman"},
            {path: "jacob",
            string: "Jacob"},
            {path: "jarom",
            string: "Jarom"},
            {path: "morm",
            string: "Mormon"},
            {path: "moro",
            string: "Moroni"},
            {path: "mosiah",
            string: "Mosiah"},
            {path: "omni",
            string: "Omni"},
            {path: "w-of-m",
            string: "Words of Mormon"},
            {path: "abr",
            string: "Abraham"},
            {path: "a-of-f",
            string: "Articles of Faith"},
            {path: "js-h",
            string: "Joseph Smith--History"},
            {path: "js-m",
            string: "Joseph Smith--Matthew"},
            {path: "moses",
            string: "Moses"}
        ]

        const relevantFootnoteReferences = footnoteReferences.filter((e) => Number(e.verseNumber) === Number(verseNumber)).map((e,i) =>{
           let volumeBookVerse = e.hrefs.replace(pathRemove, "") 
           const modifiedPathway = volumeBookVerse.split('/').map((pathway) => {
                pathsToString.forEach((object) => {
                    if(pathway === object.path){
                        pathway = object.string
                    }
                })
                return pathway
            }).join('/')
            
            return `${e.footnoteMarkers}: ${e.footnoteText} | ${modifiedPathway}`
        }).join(',;, ');

        return relevantFootnoteReferences ? relevantFootnoteReferences : null
    }
    // organizeFootnoteReferences(footnoteReferences, 13);

    
    // Need to then target New Testament, Book of Mormon, Doctrine and Covenants, and Pearl of Great Price
    if(firstBookInVolume === "Genesis"){
        booksArray = [];
        TestamentCreation(newTestamentPath);
    } else if (firstBookInVolume === "Matthew"){
        booksArray = [];
        TestamentCreation(bookOfMormonPath);
    } else if (firstBookInVolume === "1 Nephi"){
        booksArray = [];
        TestamentCreation(doctrineAndCovenants);
    } else if (firstBookInVolume === "Doctrine and Covenants"){
        console.log('Pearl of Great Price starting?')
        booksArray = [];
        TestamentCreation(pearlOfGreatPrice);
    } 
    else if (firstBookInVolume === "Moses"){
        booksArray = [];
        fs.appendFileSync('./lds_verses_and_footnotes.sql', lastInsertions, (error) => {
            if (error) throw error;
            console.log("Error?")
        })
    }
}





function TestamentCreation(pathway){
// Old testament books and verses organization
chapterFiles = []
console.log(pathway)
fs.readdirSync(pathway).forEach(file => {
    console.log(pathway, file, 'reading dir sync')
    const path = pathway + file + '/',
    bookName = file,
    bookOrder = pathway !== "OtherTexts/" ? booksInOrder.filter((e) => e.title === bookName)[0].order : "Intro, Title Page, OD1, or OD2"

    booksArray.push({bookName, path, chapterFiles, bookOrder});
  });

booksArray.forEach((e) => {
    const chapters = fs.readdirSync(e.path).filter((e) => e.includes('.html')).sort(sortNumber);
    e.chapterFiles = chapters;
})

booksArray.sort((a,b) => a.bookOrder - b.bookOrder)
console.table(booksArray)
ScrapeVolume(booksArray);
}


createSQLFile();




